package Aula_04;

import javax.swing.text.StyledEditorKit.BoldAction;

public class Conta {
	
	//Atributo static
	
	static int contador = 0000 ;

	// Atributos de Inst�ncia	
	
		private int Numero;
		private double Saldo;
		private Boolean Status;	
		private int Num;
		
		
	// Construtor
		
		public Conta( double saldo) {
			
			this.Numero = ++contador;
			this.Saldo = saldo;
		}
		public Conta(double saldo, Boolean status) {
		
			this.Numero = ++contador;
			this.Saldo = saldo;
			this.Status = status;
		}
		
		
	
		
	
		
	//M�todos de acesso publico
		
		
		public int getNumero() {
			return Numero;
		}
		public void setNumero(int numero) {
			this.Numero = numero;
		}
		public double getSaldo() {
			return Saldo;
		}
		public void setSaldo(double saldo) {
			this.Saldo = saldo;
		}
		public Boolean getStatus() {
			return Status;
		}
		public void setStatus(Boolean status) {
			Status = status;
		}
		
		//M�todos de neg�cio
		
		public void sacar(double valor) {
			
			this.Saldo -= valor;
			
		}
		public void depositar(double valor) {
			
			this.Saldo += valor;
			
		}
		
		
		
		
	}
		
		
		
		
		
		


