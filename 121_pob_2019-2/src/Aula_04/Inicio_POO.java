package Aula_04;

public class Inicio_POO {

	public static void main(String[] args) {
		
		Conta c1 = new Conta(1000, true);
		Conta c2 = new Conta(2000, true);
		
		//conta C1
		
		System.out.println("N�mero da conta " + c1.getNumero());
		System.out.println("Saldo da conta " + c1.getSaldo());
		
		
		c1.depositar(100);
		
		System.out.println("N�mero da conta " + c1.getNumero());
		System.out.println("Saldo da conta " + c1.getSaldo());

		
		System.out.println("**************************************************");
		//conta C2
		
		System.out.println("N�mero da conta " + c2.getNumero());
		System.out.println("Saldo da conta " + c2.getSaldo());
		
		c1.depositar(200);
		
		System.out.println("N�mero da conta " + c2.getNumero());
		System.out.println("Saldo da conta " + c2.getSaldo());
		
		
		
	}

}
