package Aula_05;

public class corrente extends Conta {

	private double Limite;
	
	public corrente(int numero, double saldo, double limite) {
		
		super(numero, saldo);
		this.Limite = limite;
	}
	//Construtor
	
	@Override
	public void sacar(double valor) {
		
		this.setSaldo(this.getSaldo() - (valor * Conta.TAXA) );
	}
	 
	@Override
	public void depositar(double valor) {
		
		this.setSaldo(this.getSaldo() + valor );		
	}
	
	
	//Metodos de acesso
	
	public double getLimite() {
		return Limite;
	}

	public void setLimite(double limite) {
		this.Limite = limite;
	}

}
