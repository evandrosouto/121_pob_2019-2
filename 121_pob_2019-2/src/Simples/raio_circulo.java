package Simples;

import java.util.Scanner;//Para rodar a classe scanner precisa importar isso.

public class raio_circulo {

	public static void main(String[] args) {
		
		double raio, area;
		double pi = 3.14;
		
		Scanner receber_valor = new Scanner(System.in);
		
		
		System.out.println("Digite o valor do raio.");
		System.out.print("Raio: ");
		raio = receber_valor.nextDouble();
		
		area = pi * Math.pow(raio, 2);
		
		System.out.println("O valor da area � " + area);
	}

}
	